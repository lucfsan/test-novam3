<?php

/* 
 * Nova M3.
 * Teste de Desenvolvimento
 * Autor: Luciano Santos.
 * E-mail: lucferrsan@gmail.com.
 * Arquivo: index.php
 */


require './class.php';

header("Content-type: Application/json");

$obj = new Categorias();
$categoria = null;
$option = null;

if(isset($_GET['opt']) || !empty($_GET['opt']) || !empty($_GET['ID']) || isset($_GET['ID']) != ''){
    
    
    $option = $_GET['opt'];
    switch ($option)
    {
        case "view":
            $categoria = $_GET['ID'];
            return $obj->viewCategoria($categoria);
            break;  
        case "add":
            return $obj->addCategoria();
            break;
        case "update":
            $categoria = $_GET['ID'];
            return $obj->updateCategoria($categoria);
            break;
        case "delete":
            $categoria = $_GET['ID'];
            return $obj->delCategoria($categoria);
            break;
        default:
            echo json_encode(
                    array('msg' => 'sem parâmetros definidos!')
                    );
            break;
    }
} else {
    return $obj->getCategorias();
}