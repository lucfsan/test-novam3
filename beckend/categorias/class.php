<?php

/* 
 * Nova M3.
 * Teste de Desenvolvimento
 * Autor: Luciano Santos.
 * E-mail: lucferrsan@gmail.com.
 * Arquivo: class.php
 */


require '../config/main.php';


//Classe Categorias
class Categorias {

    /*
     * Construtor
     */
    public function __construct() {

    }
    
    /*
     * @see getCategoria()
     * Método lista categorias
     */
    public function getCategorias(){
                
        $db = new Database();
        $PDO = $db->connect();
        
        $sql = "SELECT * FROM categorias";
        $stmt = $PDO->prepare($sql);
        $stmt->execute();
        
        $res = $stmt->fetchAll();
        
        //Adicionar registro nos logs
        $log = new Logs();
        return $log->addLog("get", "listou todas as categorias");
        
        if(count($res) != 0){
            foreach ($res as $value){              
                $items = array($value);
                echo json_encode($items);
            }     
        } else {
            $retorno = array('msg' => 'sem categorias');
            echo json_encode($retorno);
        }
    }
    
    /*
     * @see viewCategoria()
     * Método detalhe da categoria
     */    
    public function viewProduto($categoria){
                           
        $db = new Database();
        $PDO = $db->connect();
        
        $sql = "SELECT * FROM categorias WHERE id_categoria = '$categoria'";
        $stmt = $PDO->prepare($sql);
        $stmt->execute();
        
        //Adicionar registro nos logs
        $log = new Logs();
        return $log->addLog("view", "viu categoria: ".$categoria);        
        
        $res = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $item = array(
            'id_categoria'    => $res['id_categoria'],
            'nome_produto'  => $res['nome_produto'],
            'cod_produto'  => $res['cod_produto']
        );
        
        echo json_encode($item);
  

            
    }
       
    /*
     * @see addCategoria()
     * Método adicionar categoria
     */
    public function addCategoria(){
        
        try {
            $db = new Database();
            $PDO = $db->connect();

            $sql = "INSERT INTO categorias (nome_categoria, cod_categoria) 
                    VALUES (
                        '".$_GET['nome_categoria']."',
                        '".$_GET['cod_categoria']."')";
            $stmt = $PDO->prepare($sql);
            $stmt->execute();
        
            //Adicionar registro nos logs        
            $log = new Logs();
            return $log->addLog("insert", "criou categoria: ".$_GET['nome_categoria']);
            
            if($stmt != false){

                $retorno = array('message'=>'insert success');
                echo json_encode($retorno);
            } else {
                 $retorno = array('message'=>'insert error');
                echo json_encode($retorno);
            }


        } catch(PDOException $m) {
            $retorno = array('message'=> $m);
            echo json_encode($retorno);
        }

        

    }
    
    /*
     * @see updateCategoria()
     * Método atualiza uma categoria
     */   
    
    public function updateCategoria($categoria){
        try {
            $db = new Database();
            $PDO = $db->connect();

            $sql = "UPDATE categorias SET 
                        nome_categoria =  '".$_GET['nome_categoria']."', 
                        cod_categoria =   '".$_GET['cod_categoria']."'
                    WHERE 
                        id_categoria =    '".$categoria."'";
            $stmt = $PDO->prepare($sql);
            $stmt->execute();
            
            //Adicionar registro nos logs
            $log = new Logs();
            return $log->addLog("update", "atualizou categoria: ".$_GET['nome_categoria']);

            if($stmt->rowCount() > 0){
                $retorno = array('message'=>'update success');
                echo json_encode($retorno);
            } else {
                 $retorno = array('message'=>'update error');
                echo json_encode($retorno);
            }
        } catch(PDOException $m) {
            $retorno = array('message'=> $m);
            echo json_encode($retorno);
        }
    }
    
    /*
     * @see delCategoria()
     * Método deleta uma categoria
     */   
    public function delCategoria($categoria) {
        
        try {
            $db = new Database();
            $PDO = $db->connect();

            $sql = "DELETE FROM categorias WHERE id_categoria = '".$categoria."'";
            $stmt = $PDO->prepare($sql);
            $stmt->execute();

            //Adicionar registro nos logs
            $log = new Logs();
            return $log->addLog("delete", "deletou categoria: ".$categoria);
            
            if($stmt->rowCount() > 0){
                $retorno = array('message'=>'delete success');
                echo json_encode($retorno);
            } else {
                 $retorno = array('message'=>'delete error');
                echo json_encode($retorno);
            }
        } catch(PDOException $m) {
            $retorno = array('message'=> $m);
            echo json_encode($retorno);
        }
        
    }

}

