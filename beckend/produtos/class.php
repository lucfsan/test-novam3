<?php

/* 
 * Nova M3.
 * Teste de Desenvolvimento
 * Autor: Luciano Santos.
 * E-mail: lucferrsan@gmail.com.
 * Arquivo: class.php
 */


require '../config/main.php';

//Classe Produtos
class Produtos {

    /*
     * Construtor
     */
    public function __construct() {

    }
    
    /*
     * @see getProduto()
     * Método lista produtos
     */
    public function getProdutos(){
                
        $db = new Database();
        $PDO = $db->connect();
        
        $sql = "SELECT * FROM produtos";
        $stmt = $PDO->prepare($sql);
        $stmt->execute();
        
        $res = $stmt->fetchAll();
        
        $log = new Logs();
        return $log->addLog("get", "listou todos os produtos");
        
        if(count($res) != 0){
            foreach ($res as $value){              
                $items = array($value);
                echo json_encode($items);
            }     
        } else {
            $retorno = array('msg' => 'sem produtos');
            echo json_encode($retorno);
        }
    }
    
    /*
     * @see viewProduto()
     * Método detalhe do produto
     */    
    public function viewProduto($produto){
                           
        $db = new Database();
        $PDO = $db->connect();
        
        $sql = "SELECT * FROM produtos WHERE id_produto = '$produto'";
        $stmt = $PDO->prepare($sql);
        $stmt->execute();
        
        $log = new Logs();
        return $log->addLog("view", "viu pruduto: ".$produto);
        
        $res = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $item = array(
            'id_produto'    => $res['id_produto'],
            'nome_produto'  => $res['nome_produto'],
            'prec_produto'  => $res['prec_produto'],
            'qua_produto'   => $res['qua_produto'],
            'desc_produto'  => $res['desc_produto']
        );
        
        echo json_encode($item);
  

            
    }
       
    /*
     * @see addProduto()
     * Método adicionar protudo
     */
    public function addProduto(){
        
        try {
            $db = new Database();
            $PDO = $db->connect();

            $sql = "INSERT INTO produtos (nome_produto, cod_produto, prec_produto, qua_produto, desc_produto, cat_produto) 
                    VALUES (
                        '".$_GET['nome_produto']."',
                        '".$_GET['cod_produto']."', 
                        '".$_GET['prec_produto']."', 
                        '".$_GET['qua_produto']."', 
                        '".$_GET[ 'desc_produto']."',
                        '".$_GET[ 'cat_produto']."')";
            $stmt = $PDO->prepare($sql);
            $stmt->execute();
            
            // Adiciona registro nos logs
            $log = new Logs();
            return $log->addLog("insert", "criou produto: ".$_GET['nome_produto']);
            
            if($stmt != false){
                $retorno = array('message'=>'insert success');
                echo json_encode($retorno);
            } else {
                 $retorno = array('message'=>'insert error');
                echo json_encode($retorno);
            }


        } catch(PDOException $m) {
            $retorno = array('message'=> $m);
            echo json_encode($retorno);
        }
            
        

    }
    
    public function updateProduto($produto){
        try {
            $db = new Database();
            $PDO = $db->connect();

            $sql = "UPDATE produtos SET 
                        nome_produto =  '".$_GET['nome_produto']."', 
                        cod_produto =   '".$_GET['cod_produto']."',
                        prec_produto =  '".$_GET['prec_produto']."',
                        qua_produto =   '".$_GET['qua_produto']."',
                        desc_produto =  '".$_GET['desc_produto']."',
                        cat_produto =   '".$_GET['cat_produto']."'
                    WHERE 
                        id_produto =    '".$produto."'";
            $stmt = $PDO->prepare($sql);
            $stmt->execute();
            
            // Adiciona registro nos logs
            $log = new Logs();
            return $log->addLog("update", "atualizou produto: ".$_GET['nome_produto']);

            if($stmt->rowCount() > 0){
                $retorno = array('message'=>'update success');
                echo json_encode($retorno);
            } else {
                 $retorno = array('message'=>'update error');
                echo json_encode($retorno);
            }
        } catch(PDOException $m) {
            $retorno = array('message'=> $m);
            echo json_encode($retorno);
        }
    }
    
    public function delProduto($produto) {
        
        try {
            $db = new Database();
            $PDO = $db->connect();

            $sql = "DELETE FROM produtos WHERE id_produto = '".$produto."'";
            $stmt = $PDO->prepare($sql);
            $stmt->execute();
            
            // Adiciona registro nos logs
            $log = new Logs();
            return $log->addLog("delete", "deletou produto: ".$produto);

            if($stmt->rowCount() > 0){
                $retorno = array('message'=>'delete success');
                echo json_encode($retorno);
            } else {
                 $retorno = array('message'=>'delete error');
                echo json_encode($retorno);
            }
        } catch(PDOException $m) {
            $retorno = array('message'=> $m);
            echo json_encode($retorno);
        }
        
    }

}

