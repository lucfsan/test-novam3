<?php

/* 
 * Nova M3.
 * Teste de Desenvolvimento
 * Autor: Luciano Santos.
 * E-mail: lucferrsan@gmail.com.
 * Arquivo: index.php
 */


require './class.php';

header("Content-type: Application/json");

$obj = new Produtos();
$produto = null;
$option = null;

if(isset($_GET['opt']) || !empty($_GET['opt']) || !empty($_GET['ID']) || isset($_GET['ID']) != ''){
    
    
    $option = $_GET['opt'];
    switch ($option)
    {
        case "view":
            $produto = $_GET['ID'];
            return $obj->viewProduto($produto);
            break;  
        case "add":
            return $obj->addProduto();
            break;
        case "update":
            $produto = $_GET['ID'];
            return $obj->updateProduto($produto);
            break;
        case "delete":
            $produto = $_GET['ID'];
            return $obj->delProduto($produto);
            break;
        default:
            echo json_encode(
                    array('msg' => 'sem parâmetros definidos!')
                    );
            break;
    }
} else {
    return $obj->getProdutos();
}